#!../../bin/linux-x86_64/tbl

## You may have to change tbl to something else
## everywhere it appears in this file

epicsEnvSet("BL", "PINK")
epicsEnvSet("DEV", "TBL")

#epicsEnvSet("M0X","MSIM2:m4")
#epicsEnvSet("M0Y","MSIM2:m1")
#epicsEnvSet("M1Y","MSIM2:m2")
#epicsEnvSet("M2X","MSIM2:m5")
#epicsEnvSet("M2Y","MSIM2:m3")
#epicsEnvSet("M2Z","MSIM2:m6")
#epicsEnvSet("M2Z","junk")

epicsEnvSet("M0X","PHY:AxisE")
epicsEnvSet("M0Y","PHY:AxisD")
epicsEnvSet("M1Y","PHY:AxisC")
epicsEnvSet("M2X","PHY:AxisB")
epicsEnvSet("M2Y","PHY:AxisA")
epicsEnvSet("M2Z","junk")


< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/tbl.dbd"
tbl_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/table.db","P=PINK:,Q=$(DEV),T=$(DEV),M0X=$(M0X),M0Y=$(M0Y),M1Y=$(M1Y),M2X=$(M2X),M2Y=$(M2Y),M2Z=$(M2Z),GEOM=NEWPORT")
dbLoadRecords("db/tblpink.db","BL=PINK,DEV=$(DEV),M0X=$(M0X),M0Y=$(M0Y),M1Y=$(M1Y),M2X=$(M2X),M2Y=$(M2Y),M2Z=$(M2Z)")
#dbLoadRecords("db/tbl_alarm.db","BL=PINK,DEV=$(DEV)")
#dbLoadRecords("db/deviocstats.db","BL=PINK,DEV=$(DEV)")

cd "${TOP}/iocBoot/${IOC}"

set_savefile_path("/EPICS/autosave")
set_pass1_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK,DEV=$(DEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"

epicsThreadSleep(1.0)
dbpf("$(BL):$(DEV).PROC", "1")
