# pink-table

EPICS IOC to control optical table

## docker-composer.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-bl/pink-table/table:v1.0
    container_name: table
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: "/EPICS/IOCs/pink-table/iocBoot/ioctbl"
    command: "./tbl.cmd"
    volumes:
      - type: bind
        source: /EPICS/autosave/table
        target: /EPICS/autosave
```
